#!/bin/sh
#
# service        Startup script for the CMS DCA Notebooks service
#

cd "$(dirname "$0")"

det=$1
prt=$2
com=$3

runas=dcaser
name=dca-nbs_${det}
pidfile="/var/run/${name}.pid"
logfile="/var/log/${name}.log"

#  --NotebookApp.base_url=/nbs_${det}/ \

chown -R $runas /opt/dca-nbs/config
export JUPYTER_CONFIG_DIR=/opt/dca-nbs/config

script="jupyter-notebook \
  --notebook-dir=/opt/dca-nbs/notebooks/${det}/ \
  --NotebookApp.base_url='nbs_${det}' \
  --port ${prt}"

do_start() {
    if [ -f $pidfile ] && kill -0 $(cat $pidfile); then
        echo 'Service already running' >&2
        return 1
    fi
    touch $logfile && chown $runas $logfile
    local CMD="$script &>> \"$logfile\" & echo \$!"
    su -s /bin/bash -c "$CMD" $runas > "$pidfile"
    RETVAL=$?
    return $RETVAL
}

do_stop() {
    if [ ! -f "$pidfile" ] || ! kill -0 $(cat "$pidfile"); then
        echo 'Service not running' >&2
        return 1
    fi
    kill -15 $(cat "$pidfile") && rm -f "$pidfile"
    RETVAL=$?
    return $RETVAL
}

echo "$name $com"
case "$com" in

    start)
        do_start
	RETVAL=$?
        ;;

    stop)
        do_stop
	RETVAL=$?
        ;;

    *)
        echo "Usage: $(dirname "$0")/$(basename "$0") {start|stop}"
        RETVAL=1
        ;;

esac
exit $RETVAL
