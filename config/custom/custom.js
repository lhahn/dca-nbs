
$(Jupyter.events).on("kernel_ready.Kernel", function () {

  $(".output div").removeClass("output_scroll");
  $(".out_prompt_overlay, .prompt, .btn, .btn-default, .output_collapsed, .cell, .code_cell").unbind().prop('title', '');

  Jupyter.actions.call('jupyter-notebook:run-all-cells');

});

$(Jupyter.events).on("kernel_idle.Kernel", function (e, a) {

  $(".cell.selected").removeClass("selected").addClass("unselected");
  $("div.output:not(:empty)").parent("div.output_wrapper").parent("div.code_cell").show();
  $("div.output:empty").parent("div.output_wrapper").parent("div.code_cell").hide();

});

window.onbeforeunload = function () {

  Jupyter.notebook.kernel.kill();

};

define(['base/js/namespace'], function(Jupyter){
    Jupyter._target = '_self';
});

$(document).ready(function() {

/*
  $('<div id="show_code" class="notification_widget btn btn-xs navbar-btn" role="button"><span>Show code</span></div>').click(function (){ 
    var s = $("span", this);
    if (s.html() == "Show code") {
      $("div.code_cell,div.input").show();
      s.html("Hide code");
    } else {
      $("div.input").hide();
      $("div.output:empty").parent("div.output_wrapper").parent("div.code_cell").hide();
      s.html("Show code");
    }
  }).appendTo("div.end_space");
*/

});

/*
$("#header").hide();
$(window).unload(function() {
  Jupyter.notebook.kernel.kill();
});
*/

